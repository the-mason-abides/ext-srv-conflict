using {
  cuid,
  Country
} from '@sap/cds/common';

namespace tma.app2;

entity AppTwo : cuid {
  count: Integer;
  country : Country;
}
