using { tma.app2 as app2 } from '../db/schema';

@path : 'service/app-2'

service AppTwoService {
  entity AppTwo as projection on app2.AppTwo;
}