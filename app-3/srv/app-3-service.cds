using {
  Country
} from '@sap/cds/common';

@path : 'service/app-3'
service AppThreeService {
  entity AppThree {
    ID : UUID;
    count : Integer;
    country_code: String(3);
  }
}