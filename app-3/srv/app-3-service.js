const cds = require("@sap/cds");

module.exports = cds.service.impl(async function (srv) {
  srv.on(`READ`, `AppThree`, async (req) => {

    appOne = await cds.connect.to("AppOneService");

    appTwo = await cds.connect.to("AppTwoService");

    return {
      ID: `20466922-7d57-4e76-b14c-e53fd97dcb10`,
      count: 1,
      country_code: `GB`,
    };
  });
});
