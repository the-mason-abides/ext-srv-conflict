using { tma.app1 as app1 } from '../db/schema';

@path : 'service/app-1'

service AppOneService {
  entity AppOne as projection on app1.AppOne;
}