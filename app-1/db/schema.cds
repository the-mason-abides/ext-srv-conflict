using {
  cuid,
  Country
} from '@sap/cds/common';

namespace tma.app1;

//type Country : Association to  Countries;

entity AppOne : cuid {
  count: Integer;
  country : Country;
}
